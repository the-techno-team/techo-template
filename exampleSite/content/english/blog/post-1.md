---
title: "1. Introduction"
image: "images/blog/intro-resize.png"
description: "This is meta description."
draft: false
---

Techno Calculator Project Introduction

1. What made you want to do this project in particular?
    Our team was interested in exploring the capabilities of introductory python computing language to develop a tangible output. After thought and discussion, we decided to pursue the creation of a calculator interface.
2. What did you hope to learn?
    Throughout this project, we hope to learn how to code using python language as well as applications of the PyQt6 module.
3. What difficulties or challenges did you anticipate?
    We anticipated facing difficulty with creating funcitoning and responsive buttons for the calculator. In addition, it was challenging to begin the project with limited coding knowledge.
