---
title: "2. Plans"
image: "images/blog/blueprint-resize.png"
description: "This is meta description."
draft: false
---

Our group wanted to implement the Python programming language to build a calculator. With not much prior coding knowledge, we needed to find resources to aid us in the process. We began by looking at Professor Wu's code of creating a interactive widget that displays an interface and allows a user to push buttons. Then, we used Python's website below to further our process:

https://realpython.com/python-pyqt-gui-calculator/#creating-a-calculator-app-with-python-and-pyqt

Through the combination of our two resources and the knowledge we gained throughout the project. We were able to sucessfully create GUI calculator.

